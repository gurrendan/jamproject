﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public float thrust;
    public float rotateSpeed;
    public float maxSpeed;

    public string showVelocityX;
    public string showVelocityY;
    public float distanceToLava;
    public float distanceToLavaMin;
    public float distanceToLavaMax;
    public float distanceToLavaModifier;
    public float distanceToLavaPointsModifier;
    public float shaking;
    public float shakingCurrent;
    public float shakingMax;

    public float fuel;
    public float fuelRegen;
    public float fuelRestoration;
    public float fuelModifier;

    public LavaController lava;
    public uiController ui;
    public Camera gameCamera;

    public int hp;

    private float points;
    private float pointsPlus;

    private Rigidbody2D rigidbody2d;

    // Start is called before the first frame update
    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();

        rotateSpeed = 0.8f; //скорость поворота
        maxSpeed = 3.5f; //макс скорость
        thrust = 30f; //макс ускорение

        shaking = 0.2f; //рандомное поворачивание
        shakingCurrent = 0f; //текущее рандомное поворачивание
        shakingMax = rotateSpeed - 0.1f; //макс рандомное поворачивание


        distanceToLavaMin = 60f; //дистанция, на которой притягивает
        distanceToLavaMax = 50f; //дистанция, ЗА которой притягивает
        distanceToLavaModifier = 30f; //число, на которое делится сила притяжения
        distanceToLavaPointsModifier = 20f; //получаемые очки

        hp = 6;//хп

        fuel = 100f; //топливо
        fuelRegen = 0.01f; //топливо
        fuelRestoration = 25f; //топливо
        fuelModifier = 0.05f; //расход топлива

        lava = GameObject.FindGameObjectWithTag("Finish").GetComponent<LavaController>();
        gameCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        rigidbody2d.freezeRotation = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        gameCamera.transform.position = new Vector3(transform.position.x, transform.position.y, -10);

        rigidbody2d.velocity = Vector2.ClampMagnitude(rigidbody2d.velocity, maxSpeed);

        if (Input.GetKey(KeyCode.W) && fuel > 0 && hp > 0)
        {
            rigidbody2d.AddForce(transform.up * thrust);
            fuel -= fuelModifier;
            this.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            this.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }
        rigidbody2d.rotation += shakingCurrent;

        if (Input.GetKey(KeyCode.D) && fuel > 0 && hp > 0)
        {
            rigidbody2d.rotation -= rotateSpeed;
            this.gameObject.transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            this.gameObject.transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
        }


        if (Input.GetKey(KeyCode.A) && fuel > 0 && hp > 0)
        {
            rigidbody2d.rotation += rotateSpeed;
            this.gameObject.transform.GetChild(3).GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            this.gameObject.transform.GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (fuel < 0f)
        {
            fuel = 0;
        }

        if (Random.Range(0, 100) > 95)
        {
            shakingCurrent += Random.Range(-shaking, shaking);
            if (shakingCurrent < -shakingMax)
            {
                shakingCurrent = -shakingMax;
            }
            if (shakingCurrent > shakingMax)
            {
                shakingCurrent = shakingMax;
            }
        }

        showVelocityX = rigidbody2d.velocity.x.ToString();
        showVelocityY = rigidbody2d.velocity.y.ToString();

        distanceToLava = Vector2.Distance(this.transform.position, lava.transform.position);
        if (distanceToLava < distanceToLavaMin)
        {
            rigidbody2d.AddForce((lava.transform.position - transform.position) * (distanceToLavaMin - distanceToLava) / distanceToLavaModifier);

            pointsPlus = Mathf.Pow(distanceToLavaPointsModifier * (distanceToLavaMin - distanceToLava) / distanceToLavaModifier, 2);
            points += pointsPlus;

            this.ui.SetResearchSpeed(pointsPlus);

            this.ui.SetPoints(Mathf.FloorToInt(points / 100), Mathf.FloorToInt(pointsPlus), Mathf.FloorToInt(fuel));
        }
        else
        if (distanceToLava > distanceToLavaMax)
        {
            rigidbody2d.AddForce((lava.transform.position - transform.position) * (distanceToLava - distanceToLavaMax) / distanceToLavaModifier);
        }

        if (fuel > 3f) {
            fuel += fuelRegen;
        }

        if (fuel > 100f)
        {
            fuel = 100f;
        }

        if (Mathf.FloorToInt(fuel) == 0) {
            ui.SetAlarm(2);
        } else if (fuel < 33f) {
            ui.SetAlarm(1);
        } else {
            ui.SetAlarm(0);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            GameOver();
        }
    }

    void GameOver()
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;

        gameSystem.Instance.researchPointsLast = Mathf.FloorToInt(points / 100);
        if (Mathf.FloorToInt(points / 100) > gameSystem.Instance.researchPointsRecord)
        {
            gameSystem.Instance.researchPointsRecord = Mathf.FloorToInt(points / 100);
        }

        gameSystem.Instance.LoadScene("mainMenu");
    }

    void OnParticleCollision(GameObject other)
    {
        Debug.Log(name + " collided with " + other.name);
        switch (other.name) {
            case "Stones":
                hp--;
                this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("hp", hp);
                break;
            case "Ice":
                if (fuel < 100f) {
                    fuel += fuelRestoration;
                }
                break;
            case "Steel":
                if (hp < 6) {
                    hp++;
                }
                this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("hp", hp);
                break;
        }
    }
}
