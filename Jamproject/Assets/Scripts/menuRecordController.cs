﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuRecordController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Text>().text = "Test";
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Text>().text = "Research points record: " + gameSystem.Instance.researchPointsRecord;
        this.GetComponent<Text>().text += "\n\nResearch points last: " + gameSystem.Instance.researchPointsLast;
    }
}
