﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiController : MonoBehaviour
{
    private Text uitext;
    private Animator ui_research;
    private Image ui_alarm;
    private Image ui_critical;
    private Image ui_deactiv;
    private Image ui_engine;
    // Start is called before the first frame update
    private void Awake()
    {
        uitext = this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        ui_research = this.gameObject.transform.GetChild(0).GetComponent<Animator>();
        ui_alarm = this.gameObject.transform.GetChild(1).GetComponent<Image>();
        ui_critical = this.gameObject.transform.GetChild(2).GetComponent<Image>();
        ui_deactiv = this.gameObject.transform.GetChild(3).GetComponent<Image>();
        ui_engine = this.gameObject.transform.GetChild(4).GetComponent<Image>();
    }

    void Start()
    {
        uitext.text = "Test";

        //ui_research.speed = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetResearchSpeed(float points)
    {
        ui_research.speed = points / 20;
    }

    public void SetPoints(int points, int mod, int fuel)
    {
        uitext.text = "" + points;
        //uitext.text += " [+" + mod + "]";
        //uitext.text += " -> Fuel: " + fuel + "%";
    }

    public void SetAlarm(int points)
    {
        ui_alarm.enabled = true;
        ui_critical.enabled = false;
        ui_engine.enabled = false;
        ui_deactiv.enabled = false;
        switch (points) {
            case 0:
                ui_alarm.enabled = false;
                break;
            case 1:
                ui_engine.enabled = true;
                ui_critical.enabled = true;
                break;
            case 2:
                ui_engine.enabled = true;
                ui_critical.enabled = true;
                ui_deactiv.enabled = true;
                break;
        }
    }
}
